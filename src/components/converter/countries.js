import React, { Component } from 'react';
import getSymbolFromCurrency from 'currency-symbol-map'
import "../../App.css"
import cc from 'currency-codes'

class Countries extends Component {
    constructor() {
        super();
        this.state = {
            countryCodes: [],
            left: '1.198|$'.split('|'),
            right: '1.198|$'.split('|'),
            leftinput: '',
            rightinput: '',
            rightmultiply: '',
            lefttext: 'US Dollar',
            righttext: 'US Dollar',
            leftsymbol: 'A',
            rightsymbol: 'A'
        };
        

    }

componentDidMount(){
    
    fetch('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml')
        .then(response => response.text())
        .then(str => (new window.DOMParser()).parseFromString(str, "text/xml"))
        .then(data => {
            const countryCodes = []; // define an empty array
            const cubes = data.getElementsByTagName("Cube")
            for( const element of cubes) {
                if (!element.getAttribute('currency')) {
                    continue;
                }
                countryCodes.push({
                    currency: element.getAttribute('currency'),
                    symbol: getSymbolFromCurrency(element.getAttribute('currency')),
                    rate: element.getAttribute('rate')
                });
                this.setState({countryCodes});   
                                           
            }
        });       
    }

convertCurrCode(curr){
    const codeObject = cc.code(curr)   
    const currName = codeObject['currency']
    return currName
}

handleChange(column, value, textname, text) {
    this.setState({[column]: value, [textname]: text}, () => {
        console.log('left: ', this.state.left[0]);
        console.log('right: ', this.state.right[0]);
        console.log('leftsymbol: ', this.state.left[1]);
        console.log('rightsymbol: ', this.state.right[1]);
        console.log('leftinput: ', this.state.leftinput);
        console.log('rightinput: ', this.state.rightinput);
        console.log('rightmultiply: ', this.state.leftinput * this.state.right[0]/this.state.left[0]);
        console.log('lefttext: ', this.state.lefttext);
        console.log('righttext: ', this.state.righttext);
    });
}    
    
    render() {       
        const options = this.state.countryCodes.map(
            ({currency, symbol, rate}) => (<option value={`${rate}|${symbol}`}>{this.convertCurrCode(currency)}</option>))
        return (
            
            <div>                
                <div className="App-column-left">
                    <p>{this.state.left[0]} {this.state.lefttext} = {this.state.right[0]} {this.state.righttext}</p>
                    <select ref="App-column-left-select" onChange={e => this.handleChange('left', e.target.value.split('|'), 'lefttext', e.target.selectedOptions[0].text)}>
                        {options}
                    </select>
                    <div className="App-column-left-currency">
                        {this.state.right[1]}
                    </div>
                    <input type="text" placeholder={this.state.rightinput * this.state.left[0]/this.state.right[0]} onChange={e => this.handleChange('leftinput', e.target.value)}></input>
                </div>

                <div className="App-column-right">
                <p>{this.state.right[0]} {this.state.righttext} = {this.state.left[0]} {this.state.lefttext}</p>
                    <select ref="App-column-right-select" onChange={e => this.handleChange('right', e.target.value.split('|'), 'righttext', e.target.selectedOptions[0].text)}>
                        {options}
                    </select>
                    <div className="App-column-right-currency">
                        {this.state.left[1]}
                    </div>
                    <input type="text" placeholder={(this.state.leftinput * this.state.right[0]/this.state.left[0]).toFixed(2)} onChange={e => this.handleChange('rightinput', e.target.value)}></input>
                </div>
            </div>
        )
    }
}


export default Countries;