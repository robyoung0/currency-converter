import React, { Component } from 'react';
import Countries from './countries';

class Converter extends Component {
    constructor() {
        super();
    }



    render() {
        return (
            <div className="bodydiv">
                <div className="titlediv">
                    <h1>Currency Converter</h1>
                    <Countries />
                </div>
            </div>
        );
    }
}

export default Converter;